# This app is still in progress.

It is made for learning purposes.

___

## Local development:

* ### copy example .env configuration
    * `cp .env.example .env`
* ### install composer dependencies

    * `composer install`

* ### start docker

    * `vendor/bin/sail up -d`

* ### migrate database

    * `vendor/bin/sail artisan migrate`

* ### generate app key

    * `vendor/bin/sail artisan key:generate`

___

## Example data:

Example data is located in seeders, please perform following command to load seeders

`vendor/bin/sail artisan db:seed` or
`php artisan db:seed` if not using sail

### Users:

* User1
    * email: admin@mail.com
    * password: password
* User2
    * email: tony@mail.com
    * password: password
